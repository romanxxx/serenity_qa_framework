package serenity.steps.CNN;

import core.ws.AbstractWsConfiguration;
import dto.CnnSearchItemDTO;
import dto.CnnSearchResults;
import io.restassured.RestAssured;

import net.thucydides.core.annotations.Step;



public class CNNApiBitcoinSteps extends AbstractWsConfiguration {

    private static final String CNN_API_URL = "https://search.api.cnn.io";
    private static final String CNN_API_SEARCH_PARAMETERS = "/content?q=bitcoin&size=10";

    @Step
    public CnnSearchResults getApiSearchBitcoinResults() {

        CnnSearchResults result =
                RestAssured.given()
                .baseUri(CNN_API_URL)
                .get(CNN_API_SEARCH_PARAMETERS)
                .as(CnnSearchResults.class);

        for (int i = 0 ; i< result.getResult().length; i++){
            CnnSearchItemDTO itemDTO = result.getResult()[i];
            itemDTO.setHeadline(itemDTO.getHeadline().replaceAll("\\s", ""));

            itemDTO.setBody(itemDTO.getBody().replaceAll("\\s", ""));
        }

        return result;
    }
}